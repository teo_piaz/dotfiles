# Path to your oh-my-zsh installation.
export ZSH=~/.dotfiles/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.dotfiles/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="robbyrussell"
#ZSH_THEME="agnoster"
#ZSH_THEME="blinks"
#ZSH_THEME="ys"
ZSH_THEME="teopiaz"
#ZSH_THEME="half-life"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
DISABLE_LS_COLORS="false"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
#DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# unset shared history
setopt no_share_history 
unsetopt share_history

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(gitfast docker autojump zsh-syntax-highlighting virtualenv  zsh-autosuggestions docker-compose)

# User configuration

export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
export PATH="$HOME/.dotfiles/bin:$PATH"
export PATH=~/.npm-global/bin:$PATH
export PATH=~/.local/bin:$PATH

# export for plugins
# export FZF_BASE=~/.dotfiles/.oh-my-zsh/plugins/fzf
# export MANPATH="/usr/local/man:$MANPATH"

# path latex

export WORKON_HOME=$HOME/.virtualenvs
source ~/.local/bin/virtualenvwrapper.sh

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

#ASAN symbolizer for address sanitizer
export ASAN_SYMBOLIZER_PATH=/usr/bin/llvm-symbolizer


# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"


#function

stmflash() {
st-flash write $1  0x8000000
}


# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias ls='ls -GFh --color=auto'
alias ll='ls -Glatr'
alias l='ll -Fh'
alias db='~/opt/backlight/display-brightness'
alias kbl='~/opt/backlight/keyboard-backlight'

#autocompletamento dello slash al tab
zstyle ':completion:*' special-dirs true

zstyle ':completion:*:*:docker:*' option-stacking yes
zstyle ':completion:*:*:docker-*:*' option-stacking yes

alias ta="tmux attach -t"
alias tns='tmux new-session -s'
alias sprunge="curl -s -F 'sprunge=<-' http://sprunge.us | pbcopy"
alias gits="git status"
alias gitsu="git status -uno" #exclude untracked

alias chrome2safari="osascript ~/.dotfiles/applescripts/chrome2safari.scpt"
alias fuck='eval $(thefuck $(fc -ln -1 | tail -n 1)); fc -R'
alias hexdump="hexdump -C"
alias gitpullall="ls | xargs -P10 -I{} git -C {} pull;"i

if [[ "$OSTYPE" =~ ^darwin ]]; then
    alias grep="ggrep"
    alias egrep="gegrep"
    # Recursively delete `.pyc` files
    alias rmpyc="find . -type f -name '*.pyc' -ls -delete"

    # Recursively delete `.DS_Store` files
    alias rmds="find . -type f -name '*.DS_Store' -ls -delete"

    # Empty the Trash on all mounted volumes and the main HDD
    # Also, clear Apple’s System Logs to improve shell startup speed
    alias emptytrash="sudo rm -rfv /Volumes/*/.Trashes; sudo rm -rfv ~/.Trash; sudo rm -rfv /private/var/log/asl/*.asl"

    # Enable aliases to be sudo’ed
    alias sudo='sudo '

    # Get OS X Software Updates, update Homebrew itself, and upgrade installed Homebrew packages
    alias update='sudo softwareupdate -i -a; brew update; brew upgrade'

    # IP addresses
    alias ip="dig +short myip.opendns.com @resolver1.opendns.com"
    alias localip="ipconfig getifaddr en1"
    alias ips="ifconfig -a | grep -o 'inet6\? \(\([0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+\)\|[a-fA-F0-9:]\+\)' | sed -e 's/inet6* //'"

    #syntax highlight cat
    alias ccat='pygmentize -g -O style=colorful,linenos=1'


fi

source ~/.aliases

alias rr="rsync -r --progress"

alias ccat='pygmentize -g -O style=colorful,linenos=1'

alias remotewireshark="ssh root@10.40.0.246 tcpdump -U - -w - 'not port 22' -i eth1.4091 | sudo wireshark -k -i -"

alias g='googler -n 7 -c ru -l ru'

# reload autocompletion
autoload -Uz compinit
compinit



if [ "$TMUX" = "" ]; then tmux attach -t base || tmux new -s base; fi




