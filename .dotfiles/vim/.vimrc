set encoding=utf-8

" first, enable status line always
set laststatus=2

" now set it up to change the status line based on mode
if version >= 700
  au InsertEnter * hi StatusLine term=reverse ctermbg=5 gui=undercurl guisp=Magenta
  au InsertLeave * hi StatusLine term=reverse ctermfg=0 ctermbg=2 gui=bold,reverse
endif

" color scheme
"let g:molokai_original = 1

syntax enable
set background=dark
let g:solarized_visibility = "high"
let g:solarized_contrast = "high"
let g:solarized_termcolors=16
"let g:solarized_termcolors=256
let g:solarized_termtrans = 1 
let &t_Co=256
colorscheme solarized



" set number " show line numbers
" set relativenumber " show relative line numbers
set number " show the current line number"

" make backspace behave in a sane manner
set backspace=indent,eol,start

"copy to system clipboard
set clipboard=unnamed


set autoindent " automatically set indent of new line
set smartindent
set exrc
set secure


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" Linebreak on 500 characters
set lbr
set tw=500

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

" auto ident
filetype indent on

""""" cursor ad User Interface

set scrolloff=3 " lines of text around the cursor

set showcmd " show incomplete commands

"Display current cursor position in lower right corner.
set ruler
"Highlight searching
set hlsearch
" case insensitive search
set ignorecase
set smartcase
set incsearch  "like modern browser

set showmatch "show matching braces
set mat=2 

"Mapping 
inoremap jk <esc>


"panel movement ctrl hjkl
nnoremap <C-J> <C-W><C->
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>


"autocmd VimEnter * :silent execute ":! osascript ~/.dotfiles/applescripts/iTermDark.scpt"
"autocmd VimLeave * :silent execute ":! osascript ~/.dotfiles/applescripts/iTermLight.scpt"





"invisible chars
set invlist
set listchars=tab:▸\ ,eol:¬,trail:⋅,extends:❯,precedes:❮
highlight SpecialKey ctermbg=none " make the highlighting of tabs less annoying
set showbreak=↪

if has('mouse')
    set mouse=a
        set ttymouse=xterm2
    endif 


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" close NERDTree after a file is opened
let g:NERDTreeQuitOnOpen=0
" show hidden files in NERDTree
let NERDTreeShowHidden=1
" remove some files by extension
let NERDTreeIgnore = ['\.js.map$']
" Toggle NERDTree
nmap <F2> :NERDTreeToggle<cr>
" expand to the path of the file in the current buffer
nmap <F3> :NERDTreeFind<cr>

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif


nmap <F8> :TagbarToggle<CR>



"let g:ctrlp_map = '<c-p>'
"let g:ctrlp_cmd = 'CtrlP'

set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux

let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  \ 'file': '\v\.(exe|so|dll)$',
  \ 'link': 'some_bad_symbolic_links',
  \ }

let g:ctrlp_user_command = 'find %s -type f'        " MacOSX/Linux
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']


" YouAutocompleteMe
"let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"

"imap <Tab> <C-P>

execute pathogen#infect()
syntax on
filetype plugin indent on
