if application "Spotify" is running then
	tell application "Spotify"
		set theName to name of the current track
		set theArtist to artist of the current track
		set theAlbum to album of the current track
		set theUrl to spotify url of the current track
		set thePosition to player position
        set theDuration to duration of the current track 

	
        set theMinutes to round (theDuration / 60) rounding down
        set theSeconds to theDuration - (theMinutes * 60) as integer
        if theSeconds < 10 then set theSeconds to "0" & theSeconds	
		set stringDur to theMinutes & ":" & theSeconds

		set thePosMinutes to round (thePosition / 60) rounding down
        set thePosSeconds to thePosition - (thePosMinutes * 60) as integer
        if thePosSeconds < 10 then set thePosSeconds to "0" & thePosSeconds
        set stringPos to thePosMinutes & ":" & thePosSeconds        

        try
			return "♫-" & theName & "-" & theArtist & " (" & stringPos &")"
		on error err
           return "error"
		end try
	end tell
end if

