#!/usr/bin/env bash

source ./func.sh

# sim-link dotfiles in home

function link_dot() {
ln -sf $DOTFILES/zsh/.zshrc $HOME
ln -sf $DOTFILES/vim/.vim $HOME
ln -sf $DOTFILES/vim/.vimrc $HOME
ln -sf $DOTFILES/tmux/.tmux.conf $HOME
}

source ./brew.sh

brew_install
brew_install_recipes



e_success "Done!"

