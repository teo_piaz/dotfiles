# Where the magic happens.
export DOTFILES=~/.dotfiles

# Logging stuff.
function e_header()   { echo  "\n\033[1m$@\033[0m"; }
function e_success()  { echo  " \033[1;32m✔\033[0m  $@"; }
function e_error()    { echo  " \033[1;31m✖\033[0m  $@"; }
function e_arrow()    { echo  " \033[1;34m➜\033[0m  $@"; }


# os function

function is_osx() {
      [[ "$OSTYPE" =~ ^darwin ]] || return 1
}
function is_ubuntu() {
      [[ "$(cat /etc/issue 2> /dev/null)" =~ Ubuntu ]] || return 1
}
function get_os() {
      for os in osx ubuntu; do
              is_$os; [[ $? == ${1:-0} ]] && echo $os
                done
}

function confirmation(){
read -p "Are you sure? (y/n)" -n 1 -r
echo    #move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
echo "Bye!"
        exit 1
fi
echo "continuing..."
return 1
}


# Given strings containing space-delimited words A and B, "setdiff A B" will
# return all words in A that do not exist in B. Arrays in bash are insane
# (and not in a good way).
# From http://stackoverflow.com/a/1617303/142339
function setdiff() {
  local debug skip a b
  if [[ "$1" == 1 ]]; then debug=1; shift; fi
  if [[ "$1" ]]; then
    local setdiffA setdiffB setdiffC
    setdiffA=($1); setdiffB=($2)
  fi
  setdiffC=()
  for a in "${setdiffA[@]}"; do
    skip=
    for b in "${setdiffB[@]}"; do
      [[ "$a" == "$b" ]] && skip=1 && break
    done
    [[ "$skip" ]] || setdiffC=("${setdiffC[@]}" "$a")
  done
  [[ "$debug" ]] && for a in setdiffA setdiffB setdiffC; do
    echo "$a ($(eval echo "\${#$a[*]}")) $(eval echo "\${$a[*]}")" 1>&2
  done
  [[ "$1" ]] && echo "${setdiffC[@]}"
}
