#!/usr/bin/env bash

source ./func.sh

is_osx || return 1


function brew_install(){

if [[ ! "$(type -P brew)" ]]; then
  e_header "Installing Homebrew"
  true | ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
  e_header "Homebrew already installed"
fi

if (brew_check_install); then
  e_header "Updating Homebrew"
  brew doctor
  brew update
  brew upgrade
fi
}

function brew_check_install(){
    [[ ! "$(type -P brew)" ]] && e_error "Homebrew failed to install." && return 1
}

# Install Homebrew recipes.
function brew_install_recipes() {
  recipes=($(setdiff "${recipes[*]}" "$(brew list)"))
  if (( ${#recipes[@]} > 0 )); then
    e_header "Installing Homebrew recipes: ${recipes[*]}"
    for recipe in "${recipes[@]}"; do
      #brew install $recipe
      echo "brew install $recipe"
    done
  fi
}

function brew_install_cask_recipes() {
  cask_recipes=($(setdiff "${cask_recipes[*]}" "$(brew cask list)"))
  if (( ${#cask_recipes[@]} > 0 )); then
    e_header "Installing Homebrew recipes: ${cask_recipes[*]}"
    for cask_recipe in "${cask_recipes[@]}"; do
      #brew cask install $recipe
      echo "brew cask install $cask_recipe"
    done
  fi
}



# Homebrew recipes
recipes=(
  autojump
  binutils
  boost
  cask
  cloc
  cmake
  coreutils
  cppcheck
  curl
  dfu-util
  ffmpeg
  gnu-tar
  gnupg
  grep
  htop-osx
  mfcuk
  nmap
  open-ocd
  sdl2_mixer
  sdl_image
  sdl_mixer
  sdl_ttf
  ssh-copy-id
  ssh-copy-id
  stlink
  swig
  the_silver_searcher
  thefuck
  tmux
  tree
  vbindiff
  vim
  weechat
  wget
  wget
)


cask_recipes=(
  alfred
  bartender
  boom
  calibre
  filezilla
  flash-player
  flux
  istat-menus
  iterm2
  keepingyouawake
  spotify
  sublime-text
  transmission
  vlc
  cakebrew
  firefox
)




echo "Installing brew"

confirmation
brew_install
brew_install_recipes
brew_install_cask_recipes


